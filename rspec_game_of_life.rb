require 'rspec'
require './Game_of_life.rb'

describe "Las reglas que rigen el juego de la vida" do
  describe "determinando cuando una celula vive o muere bajo ciertas condiciones" do
    describe "Cuando una celula tiene menos de dos vecinos" do
      it "deberia morir por baja poblacion" do
        game = GameOfLife.new()
        cel1 = Celula.new("Life", 1, 1)
        cel2 = Celula.new("Life", 1, 2)
        gameOfLife.add_cell(cel1)
        gameOfLife.add_cell(cel2)
        expected (gameOfLife.cell_life_next_generation?(cel1)).to eq false
      end
    end
    describe "Cuando una celula viva tiene al menos dos vecinos y menos de cuatro" do
      it "deberia vivir para la proxima generacion" do
        game = GameOfLife.new()
        cel1 = Celula.new("Life", 1, 1)
        cel2 = Celula.new("Life", 1, 2)
        cel3 = Celula.new("Life", 2, 1)
        gameOfLife.add_cell(cel1)
        gameOfLife.add_cell(cel2)
        gameOfLife.add_cell(cel3)
        expected (gameOfLife.cell_life_next_generation?(cel1)).to eq true
      end
    end
    describe "Cuando una celula viva tiene mas de tres vecinos" do
      it "deberia morir por sobre poblacion" do
        game = GameOfLife.new()
        cel1 = Celula.new("Life", 1, 1)
        cel2 = Celula.new("Life", 1, 2)
        cel3 = Celula.new("Life", 2, 1)
        cel4 = Celula.new("Life", 0, 1)
        cel5 = Celula.new("Life", 0, 0)
        gameOfLife.add_cell(cel1)
        gameOfLife.add_cell(cel2)
        gameOfLife.add_cell(cel3)
        gameOfLife.add_cell(cel4)
        gameOfLife.add_cell(cel5)
        expected (gameOfLife.cell_life_next_generation?(cel1)).to eq false
      end
    end
    describe "Cuando una celula muerta tiene exactamente tres vecinos" do
      it "deberia vivir por reproduccion" do
        game = GameOfLife.new()
        cel1 = Celula.new("Death", 1, 1)
        cel2 = Celula.new("Life", 1, 2)
        cel3 = Celula.new("Life", 2, 1)
        cel4 = Celula.new("Life", 0, 1)
        gameOfLife.add_cell(cel1)
        gameOfLife.add_cell(cel2)
        gameOfLife.add_cell(cel3)
        gameOfLife.add_cell(cel4)
        expected (gameOfLife.cell_life_next_generation?(cel1)).to eq true
      end
    end
  end
end
